<?php
namespace Project\Utility;

class Utility{
    
    public $message = "";
    
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/Final_Project/?section=9"){
        header("Location:".$url);
    }
    
    static public function message($message = null){
        if(is_null($message)){ // please give me message
            $_message = self::getMessage();
            return $_message;
        }else{ //please set this message
            self::setMessage($message);
        }
    }
    
    static private function getMessage(){
        
        $_message =  $_SESSION['message'];
        $_SESSION['message'] = "";
        return $_message;
    }
    
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
}