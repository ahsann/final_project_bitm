<?php
namespace Project\Backbenchers;
use \Project\Utility\Utility;


class Employe{
    
    public $id = "";
    public $name = "";
    public $employment_histories = "";
    public $photo = "";
    public $deleted_at = null;
    
    public function __construct($data = false){
        if(is_array($data) && array_key_exists("id", $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
   
		//$this->photo = $files['photo'];
        $this->name = trim($data['name']);
        $this->phone = trim($data['phone']);
		$this->email = trim($data['email']);
        $this->gender = trim($data['gender']);
        $this->company_name = trim($data['company_name']);
        $this->company_business = trim($data['company_business']);
        $this->company_location = trim($data['company_location']);
        $this->department = trim($data['department']);
        $this->department = trim($data['department']);
        $this->position = trim($data['position']);
        $this->area_of_experience = trim($data['area_of_experience']);
        $this->responsibilites = trim($data['responsibilites']);
        $this->from = trim($data['from']);
        $this->to = trim($data['to']);
        
    }
    
    public function index(){
		$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
		

	   $employment_historiess = array();
	   
        $query = "SELECT * FROM `employment_histories` WHERE deleted_at IS NULL ";
		
        $result = mysql_query($query);
		
        
        while($row = mysql_fetch_object($result)){
            $employment_historiess[] = $row;
        }
        return $employment_historiess;
		
    }

	
    public function trashed(){
        
        $employment_historiess = array();
        
		$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
        
        $query = "SELECT * FROM `employment_histories` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $employment_historiess[] = $row;
        }
        return $employment_historiess;
    }
	
    
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
		
		
		/* 	if(!file_exists("upload")){
                mkdir("upload");
			}
            $ext = strtolower(pathinfo($this->photo["name"],PATHINFO_EXTENSION));
            $file_name = basename(strtolower(substr($this->photo["name"], 0,15)));
            $path = trim($file_name.".".$ext);
            if(!file_exists("upload/".$path)){
              move_uploaded_file($this->photo["tmp_name"], "upload/".$path);
            }
		
         */
        $query = "INSERT INTO `resume`.`employment_histories` (`name`, `phone`, `email`, `gender`, `company_name`, `company_business`, `company_location`, `department`, `position`, `area_of_experience`, `responsibilites`, `from`, `to`) VALUES ('".$this->name."','".$this->phone."','".$this->email."','".$this->gender."','".$this->company_name."','".$this->company_business."','".$this->company_location."','".$this->department."','".$this->position."','".$this->area_of_experience."','".$this->responsibilites."','".$this->from."','".$this->to."')";
		
		
        $result = mysql_query($query);

		if($result){
			echo "success";
		}
		else{
			echo "sorry";
		}
        
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Inserted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('../../index.php?section=9');
    }

    public function show($id = false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
        $query = "SELECT * FROM `employment_histories` WHERE id=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;

    }

    public function update(){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
        
		
		 $query = "UPDATE `resume`.`employment_histories` SET `name`='".$this->name."',`phone`='".$this->phone."',`email`='".$this->email."',`gender`='".$this->gender."',`company_name`='".$this->company_name."',`company_business`='".$this->company_business."',`company_location`='".$this->company_location."',`department`='".$this->department."',`position`='".$this->position."',`area_of_experience`='".$this->area_of_experience."',`responsibilites`='".$this->responsibilites."',`from`='".$this->from."',`to`='".$this->to."' WHERE `employment_histories`.`id` = ".$this->id;

        //var_dump($query);die();
		$result = mysql_query($query);
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Updated Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again....</div>");
        }
        
        Utility::redirect('../../index.php?section=9');
    }

    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");

        $query = "DELETE FROM `resume`.`employment_histories` WHERE `employment_histories`.`id` = ".$id;
        $result = mysql_query($query);
		
		//var_dump($query);die();
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Deleted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('../../index.php?section=9');
    }
	
	
	public function deletemultiple($ids = array()){
       
            if(is_array($ids) && count($ids) > 0){

                 //Utility::dd($ids);
                 $_ids = implode(',',$ids);



                 $query = "DELETE FROM `resume`.`employment_histories` WHERE `employment_histories`.`id` IN($_ids) ";

                 //Utility::dd( $query);
                 $result = mysql_query($query);

                 if($result){
                     Utility::message("Data has been deleted successfully.");
                 }else{
                     Utility::message(" Opss, Error Detected. can not Delete. Please try again...");
                 }

                 Utility::redirect('index.php');
             }else{
                 Utility::message('No id avaiable. Sorry !');
                 return Utility::redirect('../../index.php?section=9');
             }
    }
	
	   

	public function trash($id = null){
			$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('../../index.php?section=9');
        }
		
		$this->id = $id;
        $this->deleted_at=time();
		
	
        $query = "UPDATE `resume`.`employment_histories` SET `deleted_at` = '".$this->deleted_at."' WHERE `employment_histories`.`id` = ".$this->id;
		
		$result=mysql_query($query);

		if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Trashed Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('../../index.php?section=9');
    }
	
	   
	public function recover($id = null){
			$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("resume") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
		
		$this->id = $id;
	
        $query = "UPDATE `resume`.`employment_histories` SET `deleted_at` = NULL WHERE `employment_histories`.`id` = ".$this->id;
		
		$result=mysql_query($query);
            
        if($result){
            Utility::message("<div class=\"message_success\">Data Has recover Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. can not Recover. Please try again...</div>");
        }
        
        Utility::redirect('../../index.php?section=9');
    }
	
	public function recovermultiple($ids = array()){
       
        if(is_array($ids) && count($ids) > 0){
            
            //Utility::dd($ids);
            $_ids = implode(',',$ids);



            $query = "UPDATE `resume`.`employment_histories` SET `deleted_at` = NULL WHERE `employment_histories`.`id` IN($_ids) ";

            //Utility::dd( $query);
            $result = mysql_query($query);

            if($result){
                Utility::message("Data has been recovered successfully.");
            }else{
                Utility::message(" Opss, Error Detected. can not recover Please try again....");
            }

            Utility::redirect('../../index.php?section=9');
        }else{
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('../../index.php?section=9');
        }
        

    }

	

}
