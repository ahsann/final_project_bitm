
<?php

ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once('../../vendor/autoload.php');

use \Project\Backbenchers\Employe;
use \Project\Utility\Utility;

$employe = new Employe();
$employes = $employe->index();
$trs = "";
?>
<?php

$slno = 0;
foreach ($employes as $ph):
    $slno++;
    $trs .="<tr>";
    $trs .="<td>" . $slno . "</td>";
	$trs .="<td>" .$ph->name."</td>";
	$trs .="<td>" .$ph->photo."</td>";
	$trs .="<td>" .$ph->phone."</td>";
	$trs .="<td>" .$ph->email."</td>";
	$trs .="<td>" .$ph->gender."</td>";
	$trs .="<td>" .$ph->company_name."</td>";
	$trs .="<td>" .$ph->company_business."</td>";
	$trs .="<td>" .$ph->company_location."</td>";
	$trs .="<td>" .$ph->department."</td>";
	$trs .="<td>" .$ph->position."</td>";
	$trs .="<td>" .$ph->area_of_experience."</td>";
	$trs .="<td>" .$ph->responsibilites."</td>";
	$trs .="<td>" .$ph->from."</td>";
	$trs .="<td>" .$ph->to."</td>";
	
    $trs .="</tr>";
endforeach;
?>
<?php

$html = <<<pavel



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Personal Resume</title>

        <!-- Bootstrap -->
        <link href="../../Resource/css/bootstrap.min.css" rel="stylesheet">



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <ins><h3 class="text-center">Resume List</h3></ins> 


                <hr>

            
                <div class="row">


                    <table class="table table-bordered text-center bg-info">
                        <thead>
                            <tr>

                                <th class="text-center">Sl.</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Photo</th>
                                <th class="text-center">Phone Number</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Gender</th>
                                <th class="text-center">Company Name</th>
                                <th class="text-center">Company Business</th>
                                <th class="text-center">Company Location</th>
                                <th class="text-center">Department</th>
                                <th class="text-center">Position</th>
                                <th class="text-center">Area Of Experience</th>
								<th class="text-center">Responsibilites</th>
                                <th class="text-center">Fromm</th>
                                <th class="text-center">To</th>

                            </tr>
                        </thead>
                        <tbody>

                     echo $trs;        
                        </tbody>
                    </table>
                </div>


        </section>

      
    </body>
</html>
pavel;
?>
<?php


require_once '../../vendor/mpdf/mpdf/mpdf.php';
$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>