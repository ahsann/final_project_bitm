<?php
include_once('../../vendor/autoload.php');
use \Project\Backbenchers\Employe;
use \Project\Utility\Utility;

$obj = new Employe();
$employe = $obj->show($_GET["id"]);

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employe Info</title>
	<link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
  </head>
  <body>
      <div align="center" class="wrapper1">
          <h1>Profile Info: </h1>
          <table class="table">
            <tr class="success">
              <td>ID : <?php echo $employe->id; ?></td>
            </tr>
			<tr class="success">
              <td>User ID : <?php echo $employe->users_id; ?></td>
            </tr>
            <tr class="info">
              <td>Photo : <?php echo $employe->photo; ?></td>
            </tr>
			<tr class="info">
              <td>Name : <?php echo $employe->name; ?></td>
            </tr>
            
			<tr class="success">
              <td>Email Address: <?php echo $employe->email; ?></td>
            </tr>
			<tr class="success">
              <td>Gender : <?php echo $employe->gender; ?></td>
            </tr>
			<tr class="success">
              <td>Company Name : <?php echo $employe->company_name; ?></td>
            </tr>
			<tr class="success">
              <td>Company Business : <?php echo $employe->company_business; ?></td>
            </tr>
			<tr class="success">
              <td>Company Location : <?php echo $employe->company_location; ?></td>
            </tr>
			<tr class="success">
              <td>Department : <?php echo $employe->department; ?></td>
            </tr>
			<tr class="success">
              <td>Position : <?php echo $employe->position; ?></td>
            </tr>			
			<tr class="success">
              <td>Area Of Experience : <?php echo $employe->area_of_experience; ?></td>
            </tr>
			<tr class="success">
              <td>Responsibilites : <?php echo $employe->responsibilites; ?></td>
            </tr>
			<tr class="success">
              <td>From : <?php echo $employe->from; ?></td>
            </tr>

			<tr class="success">
              <td>To : <?php echo $employe->to; ?></td>
            </tr>
          </table>

          <p class="text-center btn btn-success"><a href="index.php">Go to DashBoard</a></p>
          <p class="text-center btn btn-success"><a href="view.php">Go to Employee List</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>