<?php
session_start();
include_once('../../vendor/autoload.php');
use \Project\Backbenchers\Employe;
use \Project\Utility\Utility;

$employe = new Employe();
$employes = $employe->trashed();
    
    
    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trrashed </title>
	<link rel="stylesheet" href="../../Resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../Resource/style.css">
	
	<style type="text/css">

	.text_align{
		text-align:center;
	}
	</style>
	
  </head>
  <body>
		<div class="wrapper">
			<div class="header">
				<h4>Employes Info</h4>
				<div id="message">
				<?php echo Utility::message(); ?>            
				</div>
			</div>
				<form action="recovermultiple.php" method="post">
				<div class=" col-md-12">
				<div class=" col-md-7"></div>
				<div class=" col-md-2"><button class="btn btn-info btn-xs" type="submit">Recover All</button></div>
				<div class=" col-md-2"><button class="btn btn-danger btn-xs delete" type="button" id="deleteAll">Delete All</button></div>
				</div>
				<br />
				<br />
				
							
								<div class="main_content">
								
									<div class="form-horizontal">
										<div class="form-group">
											<div class="row  col-md-16">
									
												<div class="  col-md-2">
													<form class="ajax" action="#" method="post">
														<select class="items" name="items">
															<option value="15">10</option>
															<option value="20">20</option>
															<option value="30">30</option>
															<option value="40">40</option>
														</select>
													</form>
												</div>
										
												<div class=" col-md-2">
													<a  class="btn btn-success btn-xs" href="#">SEARCH</a>
												</div>
										
												<div class=" col-md-8">
																							
													<div class="row">
														<div class=" col-md-4">
																		
															<p class="textRight"></p>
														</div>
														<div class="col-md-8">
															<div class="col-md-6"></div>				
															<div class="col-md-2">
																<a class="btn btn-warning btn-xs" href="../../index.php?section=9" class="list-btn">Employes List</a>
															</div>				
																															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
							
									<div class="row main_info">
										<div class="col-md-12">
											<div class="box-table">	
												<table class="table table-border table-hover">
													<thead class="text-center">
														<tr>
															<th><input type="checkbox" name="markall" id="markall" ></th>
															<th  class="text_align">Serial</th>
															<th  class="text_align">Photo</th>
															<th  class="text_align">Name</th>
															<th  class="text_align">Number</th>
															<th  class="text_align">Position</th>
															<th  class="text_align">Action</th>
															</tr>
													</thead>
													<tbody class="text-center">
														<?php
														if(count($employes) > 0){
														   $count =1;
														   foreach($employes as $employe){
														?>
														<tr>
															<td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $employe->id;?>"></td>
															<td><?php echo $count;?></td>
															<td><a href="show.php?id=<?php echo $employe->id;?>"><?php echo $employe->photo;?></a></td>
															<td><?php echo $employe->name;?></td>
															<td><?php echo $employe->phone;?></td>
															<td><?php echo $employe->position;?></td>
														
															<td>
																<a class="btn btn-danger btn-xs" href="delete.php?id=<?php echo $employe->id;?>" class="list-btn delete">Delete</a>
																
																<a class="btn btn-warning btn-xs" href="recover.php?id=<?php echo $employe->id;?>" class="list-btn">recover</a>
																
															</td>
														</tr>
														<?php
															$count++;
															}
														}
														else{
														?>
															<tr>
																<td colspan="6">No record is available.</td>
															</tr> 
														<?php
															}
														?>
													
													</tbody>
												</table>
											</div>
					
										</div> 
					
									</div>
								</div>
				</form>
		</div>
      <p class="text-center"><a href="../../index.php">Go to Homepage</a></p>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="../../Resource/js/bootstrap.min.js"></script>
    <script>
		$(document).ready(function(){
        
        $('.delete').bind('click',function(e){
        var deleteItem = confirm("Are you sure you want to delete?");
            if(!deleteItem){
                //return false; 
                e.preventDefault();
            }
        });     
        $('#message').hide(5000);
        
        $('#markall').bind('click', function(){
            if($('#markall').is(':checked')){
                $('.mark').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "mark"               
                });
            }else{
                $('.mark').each(function() { //loop through each checkbox
                    this.checked = false;  //select all checkboxes with class "mark"               
                });
            }
        });
        
         $('#deleteAll').bind('click',function(e){
            
            var startDeleteProcess = false;
            $('.mark').each(function() { //loop through each checkbox
                  if($(this).is(':checked')){
                      startDeleteProcess = true;                
                  }       
            });
           
            if(startDeleteProcess){
                var deleteItem = confirm("Are you sure you want to delete all Items??");
                    if(deleteItem){
                        document.forms[0].action = 'deletemultiple.php';
                        document.forms[0].submit();
                    } 
            }else{
                alert("Please select an item first");
            }
            

        }); 
        
    });
    </script>
  </body>
</html>