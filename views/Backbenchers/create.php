<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Add Employe</title>

        <link href="../../Resource/css/bootstrap.min.css" rel="stylesheet">
        
        <link href="../../Resource/style.css" rel="stylesheet">
		
        
    </head>
	
	<body>
		<div class="wrapperr">
		
			<div class="header">
				<h4>Add an Employe Information</h4>
			</div>
			
			<div class="main_content">
				<div align="center"></div>
				
					<div class="editing_info">

							<!-- Nav tabs -->
							<ul class="nav nav-tabs nav_tabs" role="tablist">
							<li role="presentation" class="active"><a href="" aria-controls="create" role="tab" data-toggle="tab">Create New</a></li>
						
							</ul>

							<!-- Tab panes -->
							<div class="tab-content ">
								<div role="tabpanel" class="tab-pane fade in active" id="create">
									<form class="form-inline" action="store.php" method="post" >
										
										<div>
												<div style="float:left" class="form-group bottom_tab">
														<label for="exampleInputName2">Name</label>
														<input type="text" class="form-control" id="exampleInputName2" placeholder="write here" name="name" />
													</div style="float:left" >
													<div style="float:right" class="">
														<input style="width: 100px; height: 100px;"type="file" name="photo" class="form-control" id="field2">
														<br />
														<label for="field2">Upload Image</label>
													</div>
												
												</div>
											
											
													<div>
														<div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Mobile Number</label>
															<input type="text" class="form-control" id="exampleInputEmail2" placeholder="+880" name="phone" />
														</div>
															
														  
														 
														  
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Email Adderss</label>
															<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter Mail" name="email" />
														  </div>
														   <div class="form-group bottom_tab radio">
															  <label>Sex</label>&nbsp;&nbsp;
															  <label>
																<input type="radio" class="form-control" name="gender" value="Male" checked="checked">Male
															  </label>
																&nbsp;&nbsp;
															  <label>
																<input type="radio" class="form-control" name="gender" value="Female">Female
															  </label>
															  
															</div><br>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Company Name</label>
															<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Company Name" name="company_name" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Company Business</label>
															<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Company Business" name="company_business" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Company Location</label>
															<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Company Location" name="company_location" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Department</label>
															<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Department" name="department" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Position</label>
															<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Position" name="position" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Area Of Experience</label>
															<input type="test" class="form-control" id="exampleInputEmail2" placeholder="Area Of Experience" name="area_of_experience" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">Responsibilites</label>
															<input type="test" class="form-control" id="exampleInputEmail2" placeholder="Responsibilites" name="responsibilites" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">From</label>
															<input type="date" class="form-control" id="exampleInputEmail2" placeholder="yyyy-mm-dd" name="from" />
														  </div>
														  <div class="form-group bottom_tab">
															<label for="exampleInputEmail2">To</label>
															<input type="date" class="form-control" id="exampleInputEmail2" placeholder="yyyy-mm-dd" name="to" />
														  </div>
														  
														  
													</div><br>
												
												<div class="button">
													  <input type="reset" class="btn btn-danger">
													  <button type="submit" class="btn btn-success" name="submit">SAVE</button>
													  <a class="btn btn-info" href="../../index.php?section=9">Back TO Employee List</a>
												 
												</div>
										</div>												
									</form>
									
								</div>

							</div>
					</div>
			</div>
		</div>
		
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
	</body>
	
</html>	